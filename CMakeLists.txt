cmake_minimum_required(VERSION 3.13)

project(ra-testaddon VERSION 0.1 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

if (${CMAKE_CURRENT_SOURCE_DIR} STREQUAL ${CMAKE_SOURCE_DIR})
    if (NOT TARGET_TRIPLET)
        message(FATAL_ERROR "Not target triplet defined! Can't continue, exiting...")
    endif()

    if (CLANG_TARGET)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -target ${TARGET_TRIPLET} ${ADDITIONAL_C_FLAGS}")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -target ${TARGET_TRIPLET} ${ADDITIONAL_CXX_FLAGS}")
    endif()
endif()

set(LIBRARY_OUTPUT_PATH  ${CMAKE_CURRENT_SOURCE_DIR}/build/${TARGET_TRIPLET}/)

add_library(ra-testaddon SHARED src/test-addon.cpp)

set_target_properties(ra-testaddon PROPERTIES PREFIX "${TARGET_TRIPLET}-")