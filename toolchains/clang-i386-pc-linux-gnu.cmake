set(TARGET_TRIPLET "i386-pc-linux-gnu")
set(CLANG_TARGET ${TARGET_TRIPLET})
set(CMAKE_SYSTEM_NAME "Linux")

file(MAKE_DIRECTORY ../build)
file(MAKE_DIRECTORY ../build/${TARGET_TRIPLET})

set(ENV{CC} "/usr/bin/clang")
set(ENV{CXX} "/usr/bin/clang++")