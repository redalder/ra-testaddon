set(TARGET_TRIPLET "x86_64-w64-mingw32")
set(CMAKE_SYSTEM_NAME "Windows")
set(CMAKE_CROSSCOMPILING_EMULATOR "/usr/bin/wine")

file(MAKE_DIRECTORY ../build)
file(MAKE_DIRECTORY ../build/${TARGET_TRIPLET})

set(ENV{CC} "/usr/bin/x86_64-w64-mingw32-gcc")
set(ENV{CXX} "/usr/bin/x86_64-w64-mingw32-g++")